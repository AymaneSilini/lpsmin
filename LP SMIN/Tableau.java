
public class Tableau {

  private int [] t ;
  private int ptr ;
  private int nb ;

  public Tableau (int nb) {
    this.nb=nb;
    t=new int [nb];
    ptr=0;
  }

  public void add (int v){
      t[ptr]=v;
      ptr++;
  }

  public String toString (){
    String s="";
    for (int k=0;k<ptr;k++){
      s=s+t[k]+" ";
    }
    return s;
  }

  // retourne l'indice du plus petit element dans la table
  public int imin (){
    // attention, il faut initialiser indmin avec un indice de valeur different de -1
    int indmin=0;
    for (int i=0 ; i<ptr ; i++){
      if (t[i]!=-1){
        indmin=i;
        break;
      }
    }
    // maintenant, on recherche le minimum
    for (int i=1 ; i<ptr ; i++){
      if ((t[i]<t[indmin]) && (t[i]!=-1)){
        indmin=i;
      }
    }
    return indmin;
  }

  // retourne l'indice du plus grand element dans la table
  public int imax (){
    int indmax=0; // au pire -1, pas grave
    for (int i=1 ; i<ptr ; i++){
      if ((t[i]>t[indmax]) && (t[i]!=-1)){
        indmax=i;
      }
    }
    return indmax;
  }

  // tri par recherche du min et max
  public void TriMinMax () {
    int gauche=0;
    int droite=ptr-1;

    int [] res = new int [nb];

    while (gauche<=droite){

      // il reste juste un element a placer dans le tableau resultat
      if (gauche==droite){
        int indmin=imin();
        res[gauche]=t[indmin];
        gauche++;
        t[indmin]=-1;
      }
      // il reste plus d'un element a trier
      else
      {
        int indmin=imin();
        int indmax=imax();

        res[gauche]=t[indmin];
        gauche++;
        res[droite]=t[indmax];
        droite--;

        t[indmin]=-1;
        t[indmax]=-1;
      }
    }

    t=res;

}

 public static void main (String [] args) {
   Tableau tab=new Tableau(20);
   tab.add(3);
   tab.add(5);
   tab.add(14);
   tab.add(8);
   tab.add(12);
   tab.add(15);
   tab.add(111);
   tab.add(33);
   tab.add(144);
   System.out.println(tab);
   tab.TriMinMax();
   System.out.println(tab);

 }

}
